package com.example.uapv1700095.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    private static String City = "City";
    WeatherDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        dbHelper = new WeatherDbHelper(this);

        dbHelper.populate();
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllCities () ,
                new String[] { WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                        COLUMN_COUNTRY },
                new int[] { android.R.id.text1, android.R.id.text2},0);
        final ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(cursorAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick (AdapterView parent, View v, int position, long id)  {
                Cursor item = (Cursor) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra(City, dbHelper.cursorToCity(item));
                Log.d("Coucou", dbHelper.cursorToCity(item).getName());
                startActivity(intent);

            }
        });



        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent,1);
                //startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllCities () ,
                new String[] { WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                        COLUMN_COUNTRY },
                new int[] { android.R.id.text1, android.R.id.text2},0);
        final ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(cursorAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
